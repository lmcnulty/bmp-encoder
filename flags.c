#include <stdio.h>
#include <stdlib.h>
#include "bmp.h"

int main() {

	char fr[] = {
		0, 85, 164,    // Bleu
		255, 255, 255, // Blanc
		239, 65, 53,   // Rouge
		0, 85, 164,
		255, 255, 255,
		239, 65, 53,
	};
	write_bmp("french_flag.bmp", fr, sizeof(fr) / sizeof(char), 3);

	char bgm[] = {
		0, 0, 0,       // Black
		253, 218, 36,  // Yellow
		239, 51, 64,   // Red
		0, 0, 0,
		253, 218, 36,
		239, 51, 64,
	};
	write_bmp("belgian_flag.bmp", bgm, sizeof(bgm) / sizeof(char), 3);


	char *trans = (char*) malloc(8 * 5* sizeof(char) * 3);
	for (int row = 0; row < 5; row++) {
		for (int col = 0; col < 8; col++) {
			if (row == 0 || row == 4) {
				trans[3 * (row * 8 + col)] = 91;
				trans[3 * (row * 8 + col) + 1] = 207;
				trans[3 * (row * 8 + col) + 2] = 250;
			} else if (row == 1 || row == 3) {
				trans[3 * (row * 8 + col)] = 245;
				trans[3 * (row * 8 + col) + 1] = 171;
				trans[3 * (row * 8 + col) + 2] = 185;
			} else {
				trans[3 * (row * 8 + col)] = 255;
				trans[3 * (row * 8 + col) + 1] = 255;
				trans[3 * (row * 8 + col) + 2] = 255;
			}
		}
	}
	write_bmp("trans_flag.bmp", trans, 3*5*8, 8);


	char *uae = (char*) malloc(12 * 6* sizeof(char) * 3);
	for (int row = 0; row < 6; row++) {
		for (int col = 0; col < 12; col++) {
			if (col < 3) {
				uae[3 * (row * 12 + col)] = 255;
				uae[3 * (row * 12 + col) + 1] = 0;
				uae[3 * (row * 12 + col) + 2] = 0;
			} else if (row < 2) {
				uae[3 * (row * 12 + col)] = 0;
				uae[3 * (row * 12 + col) + 1] = 116;
				uae[3 * (row * 12 + col) + 2] = 33;
			} else if (row < 4) {
				uae[3 * (row * 12 + col)] = 255;
				uae[3 * (row * 12 + col) + 1] = 255;
				uae[3 * (row * 12 + col) + 2] = 255;
			} else {
				uae[3 * (row * 12 + col)] = 0;
				uae[3 * (row * 12 + col) + 1] = 0;
				uae[3 * (row * 12 + col) + 2] = 0;
			}
		}
	}
	write_bmp("uae_flag.bmp", uae, 3*6*12, 12);

}



